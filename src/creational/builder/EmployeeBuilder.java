package creational.builder;

public class EmployeeBuilder
{
  private String firstName;
  private String lastName;
  private String role;
  private String department;
  private int    salary;
  private int    id;
  
  public EmployeeBuilder(String firstName , String lastName , int id)
  {
    this.firstName = firstName;
    this.lastName = lastName;
    this.id =id;
  }
  public EmployeeBuilder addRole(String role)
  {
    this.role = role;
    return this;
  }
  public EmployeeBuilder addSalary(int salary)
  {
    this.salary = salary;
    return this;
  }
  
  public EmployeeBuilder addDepartment(String department)
  {
    this.department = department;
    return this;
  }
  
  public Employee build()
  {
    return new Employee(this);
  }
  
  public String getFirstName()
  {
    return firstName;
  }
  
  public String getLastName()
  {
    return lastName;
  }
  
  public String getRole()
  {
    return role;
  }
  
  public String getDepartment()
  {
    return department;
  }
  
  public int getSalary()
  {
    return salary;
  }
  
  public int getId()
  {
    return id;
  }
}
