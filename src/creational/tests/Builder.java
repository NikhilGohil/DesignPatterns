package creational.tests;

import creational.builder.Employee;
import creational.builder.EmployeeBuilder;

public class Builder
{
  public static void main(String[] args)
  {
    Employee nick = new EmployeeBuilder("Nick","Smith",1).build();
    Employee john = new EmployeeBuilder("John", "Doe",2).addDepartment("Marketing").build();
    Employee matt = new EmployeeBuilder("matt" ,"king",3).addDepartment("Sales").addRole("Manger").addSalary(10000).build();
    
    
    System.out.println(nick);
    System.out.println(john);
    System.out.println(matt);
  }
}
